package com.tembici.ian.githubjavapop.Util;

import java.util.HashMap;

/**
 * Created by nalmir on 13/07/2018.
 */

public class HMAux extends HashMap<String, String> {

    public String getName() {
        return this.get(Constantes.USERNAME);
    }

    public String getId() {
        return this.get(Constantes.ID);
    }
}
