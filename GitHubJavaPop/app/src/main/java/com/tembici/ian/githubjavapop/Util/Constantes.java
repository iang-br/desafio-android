package com.tembici.ian.githubjavapop.Util;

public class Constantes {

    public static final String TITLE = "title";
    public static final String USERNAME = "username";
    public static final String FULLNAME = "fullname";
    public static final String BODY = "body";
    public static final String ID = "id";
    public static final String DESC = "desc";
    public static final String NAME = "name";
    public static final String NBRFORKS = "nbrforks";
    public static final String NBRSTARS = "nbrstars";
    public static final String PHOTO_URL = "photo_url";
    public static final String PR_URL = "pr_url";
    public static final String HTML_URL = "html_url";
    public static final String DATE = "date";

}
