package com.tembici.ian.githubjavapop.Json.api_repositories;

import com.tembici.ian.githubjavapop.Util.Constantes;
import com.tembici.ian.githubjavapop.Util.HMAux;

public class items {

    private long id;
    private String name;
    private String full_name;
    private String description;
    private long forks_count;
    private long stargazers_count;
    private owner owner;

    public HMAux getItemHashmap(){
        HMAux item = new HMAux();
        item.put(Constantes.ID,String.valueOf(id));
        item.put(Constantes.NAME,name);
        item.put(Constantes.DESC,description);
        item.put(Constantes.NBRFORKS,String.valueOf(forks_count));
        item.put(Constantes.NBRSTARS,String.valueOf(stargazers_count));
        item.put(Constantes.USERNAME,owner.getLogin());
        item.put(Constantes.PHOTO_URL,owner.getAvatar_url());
        item.put(Constantes.PR_URL,"https://api.github.com/repos/"+full_name+"/pulls");

        return item;
    };

}
