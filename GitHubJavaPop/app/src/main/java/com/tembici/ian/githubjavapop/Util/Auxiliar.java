package com.tembici.ian.githubjavapop.Util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

public class Auxiliar {

    public static void DownloadAndSaveImageFromWeb(Context context, String url, String src) {
        if ( FileExists(context,src)) {
            // do nothing!!!
        }
        else {
            try {
                Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
                InputStream is = (InputStream) new URL(url).getContent();
                //Drawable d = Drawable.createFromStream(is, src);
                SaveImage(context, bitmap, src);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//    try {
//        ImageView i = (ImageView)findViewById(R.id.image);
//        Bitmap bitmap = BitmapFactory.decodeStream((InputStream)new URL(imageUrl).getContent());
//        i.setImageBitmap(bitmap);
//    } catch (MalformedURLException e) {
//        e.printStackTrace();
//    } catch (IOException e) {
//        e.printStackTrace();
//    }

    public static String formatDate (String date) {
        try {
            String[] split_ymd =  date.substring(0,10).split("-");
            StringBuilder f_date = new StringBuilder();
            f_date.append(split_ymd[2]);
            f_date.append("/");
            f_date.append(split_ymd[1]);
            f_date.append("/");
            f_date.append(split_ymd[0]);

            return f_date.toString();
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }


    }

    private static void SaveImage(Context context, Bitmap finalBitmap, String src) {

        String root = context.getFilesDir().getPath();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();

        String fname = "Image-"+ src +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap LoadImage (Context context, String src) {

        try {
            String root = context.getFilesDir().getPath();
            File myDir = new File(root + "/saved_images");

            return BitmapFactory.decodeFile(myDir + "/Image-"+ src +".jpg");

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean FileExists (Context context, String src) {

        String root = context.getFilesDir().getPath();
        File myDir = new File(root + "/saved_images");

        String fname = "Image-"+ src +".jpg";
        File file = new File (myDir, fname);
        return file.exists ();
    }


}
