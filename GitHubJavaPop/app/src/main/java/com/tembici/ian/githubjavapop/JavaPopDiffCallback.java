package com.tembici.ian.githubjavapop;

import android.support.v7.util.DiffUtil;

import com.tembici.ian.githubjavapop.Util.HMAux;

import java.util.List;

public class JavaPopDiffCallback extends DiffUtil.Callback {

    private List<HMAux> mOldList;
    private List<HMAux> mNewList;

    public JavaPopDiffCallback(List<HMAux> oldList, List<HMAux> newList) {
        this.mOldList = oldList;
        this.mNewList = newList;
    }
    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        // add a unique ID property on Contact and expose a getId() method
        return mOldList.get(oldItemPosition).getId() == mNewList.get(newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        HMAux oldContact = mOldList.get(oldItemPosition);
        HMAux newContact = mNewList.get(newItemPosition);

        if (oldContact.getName() == newContact.getName()) {
            return true;
        }
        return false;
    }
}