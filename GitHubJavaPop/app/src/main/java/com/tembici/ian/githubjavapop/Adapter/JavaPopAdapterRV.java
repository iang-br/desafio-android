package com.tembici.ian.githubjavapop.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tembici.ian.githubjavapop.R;
import com.tembici.ian.githubjavapop.Util.Auxiliar;
import com.tembici.ian.githubjavapop.Util.Constantes;
import com.tembici.ian.githubjavapop.Util.HMAux;

import java.util.List;

public class JavaPopAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private Context context;

    private int resource; // Layout célula
    private List<HMAux> dados; //coleção

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    public class JavaPopViewHolder extends RecyclerView.ViewHolder {

        final ImageView iv_photo;
        final TextView tv_desc;
        final TextView tv_fullname;
        final TextView tv_name;
        final TextView tv_nbrforks;
        final TextView tv_nbrstars;
        final TextView tv_username;

        public JavaPopViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_photo = itemView.findViewById(R.id.celjavapop_iv_photo);
            tv_desc = itemView.findViewById(R.id.celjavapop_tv_desc);
            tv_fullname = itemView.findViewById(R.id.celjavapop_tv_fullname);
            tv_name = itemView.findViewById(R.id.celjavapop_tv_name);
            tv_nbrforks = itemView.findViewById(R.id.celjavapop_tv_nbrforks);
            tv_nbrstars = itemView.findViewById(R.id.celjavapop_tv_nbrstars);
            tv_username = itemView.findViewById(R.id.celjavapop_tv_username);
        }
    }

    public JavaPopAdapterRV(Context context, int resource, List<HMAux> dados) {
        this.dados = dados;
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(resource, parent, false);

            return new JavaPopViewHolder(view);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.itemloading, parent, false);
            return new LoadingViewHolder(view);
        }
    }

//    @Override
//    public void onBindViewHolder(@NonNull JavaPopViewHolder nossoViewHolder, int i) {
//
//    }

    @Override
    public int getItemViewType(int position) {
        return dados.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof JavaPopViewHolder) {
            JavaPopViewHolder holder = (JavaPopViewHolder) viewHolder;

            HMAux item = dados.get(position);

            String username = item.get(Constantes.USERNAME);
            holder.iv_photo.setImageBitmap(Auxiliar.LoadImage(context,username));
            holder.tv_desc.setText(item.get(Constantes.DESC));
            holder.tv_fullname.setText(item.get(Constantes.FULLNAME));
            holder.tv_name.setText(item.get(Constantes.NAME));
            holder.tv_nbrforks.setText(item.get(Constantes.NBRFORKS));
            holder.tv_nbrstars.setText(item.get(Constantes.NBRSTARS));
            holder.tv_username.setText(username);
        } else {
            showLoadingView((LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return dados == null? 0 : dados.size();
    }

    public void refreshAdp() {
        notifyDataSetChanged();
    }
}
