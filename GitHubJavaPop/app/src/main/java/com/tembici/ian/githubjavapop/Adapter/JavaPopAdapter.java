package com.tembici.ian.githubjavapop.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tembici.ian.githubjavapop.R;
import com.tembici.ian.githubjavapop.Util.Auxiliar;
import com.tembici.ian.githubjavapop.Util.Constantes;
import com.tembici.ian.githubjavapop.Util.HMAux;

import java.util.List;

public class JavaPopAdapter extends BaseAdapter {

    private Context context;

    private int resource; // Layout célula
    private List<HMAux> dados; //coleção

    private LayoutInflater mInflater; // Le XML transforma em um Binario findViewByID

    public void refreshAdp() {
        notifyDataSetChanged();
    }

    public JavaPopAdapter(Context context, int resource, List<HMAux> dados) {
        this.context = context;
        this.resource = resource;
        this.dados = dados;
        //
        this.mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return dados.size();
    }

    @Override
    public Object getItem(int position) {
        return dados.get(position);
    }

    @Override
    public long getItemId(int position) {
        HMAux aux = dados.get(position);
        //
        return Long.parseLong(aux.get(Constantes.ID));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(resource, parent, false);
        }

        // Puxar os Dados de uma posicao especifica
        HMAux item = dados.get(position);

        // Acesso todos os controles da celula
        ImageView iv_photo = convertView.findViewById(R.id.celjavapop_iv_photo);
        TextView tv_desc = convertView.findViewById(R.id.celjavapop_tv_desc);
        TextView tv_fullname = convertView.findViewById(R.id.celjavapop_tv_fullname);
        TextView tv_name = convertView.findViewById(R.id.celjavapop_tv_name);
        TextView tv_nbrforks = convertView.findViewById(R.id.celjavapop_tv_nbrforks);
        TextView tv_nbrstars = convertView.findViewById(R.id.celjavapop_tv_nbrstars);
        TextView tv_username = convertView.findViewById(R.id.celjavapop_tv_username);

        String username = item.get(Constantes.USERNAME);
        iv_photo.setImageBitmap(Auxiliar.LoadImage(context,username));
        tv_desc.setText(item.get(Constantes.DESC));
        tv_fullname.setText(item.get(Constantes.FULLNAME));
        tv_name.setText(item.get(Constantes.NAME));
        tv_nbrforks.setText(item.get(Constantes.NBRFORKS));
        tv_nbrstars.setText(item.get(Constantes.NBRSTARS));
        tv_username.setText(username);

        return convertView;
    }
}
