package com.tembici.ian.githubjavapop.View;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.tembici.ian.githubjavapop.Adapter.JavaPopAdapterRV;
import com.tembici.ian.githubjavapop.Util.Auxiliar;
import com.tembici.ian.githubjavapop.Util.Constantes;
import com.tembici.ian.githubjavapop.Util.HMAux;
import com.tembici.ian.githubjavapop.Json.api_repositories.items;
import com.tembici.ian.githubjavapop.Json.api_repositories.repositories;
import com.tembici.ian.githubjavapop.Json.api_users.users;
import com.tembici.ian.githubjavapop.R;
import com.tembici.ian.githubjavapop.ItemClickSupport;

import java.util.ArrayList;

public class TelaJavaPop extends AppCompatActivity {

    private Context context;

    private RecyclerView rv_listjavapop;
    private JavaPopAdapterRV javaPopAdapterRV;
    private ArrayList<HMAux> rep;

    private RequestQueue queue;

    private boolean isLoading = false;

    private int actualPage = 1;
    private int currentReadName = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.telajavapop);

        initVars();
        initActions();
    }

    private void initVars() {
        rep = new ArrayList<>(); // colecao vazia

        queue = Volley.newRequestQueue(this);

        context = getBaseContext();

        rv_listjavapop = findViewById(R.id.telajavapop_rv_listpr);

        gerarReps();

        javaPopAdapterRV = new JavaPopAdapterRV(
                this,
                R.layout.celjavapop,
                rep
        );

        rv_listjavapop.setAdapter(javaPopAdapterRV);

        RecyclerView.LayoutManager layout = new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false);

        rv_listjavapop.setLayoutManager(layout);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_listjavapop.getContext(),
                DividerItemDecoration.VERTICAL);
        rv_listjavapop.addItemDecoration(dividerItemDecoration);

    }

    private void gerarReps() {
        final String url = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page="+actualPage;

        actualPage = actualPage+1;

        StringRequest getRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        cleanOnProgress();

                        // display response
                        Log.d("Response", response);

                        Gson gson = new Gson();

                        repositories repos = gson.fromJson(
                                response,
                                repositories.class
                        );

                        ArrayList<items> repos_items = repos.get_items();
                        for (int i=0; i< repos_items.size(); i++) {
                            HMAux Item = repos_items.get(i).getItemHashmap();

                            rep.add(Item);
                        }

                        javaPopAdapterRV.refreshAdp();
                        isLoading = false;

                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                for (int i=0; i< rep.size(); i++) {
                                    String photo_url = rep.get(i).get(Constantes.PHOTO_URL);
                                    String username = rep.get(i).get(Constantes.USERNAME);
                                    Auxiliar.DownloadAndSaveImageFromWeb(context, photo_url, username);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            javaPopAdapterRV.refreshAdp();
                                        }
                                    });
                                }
                            }
                        }).start();

                        // Thread com objetivo de buscar os fullnames dos clientes
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                for (int j=currentReadName; j< rep.size(); j++) {

                                    String username = rep.get(j).get(Constantes.USERNAME);

                                    if (rep.get(j).containsKey(Constantes.FULLNAME)) {
                                        continue;
                                    }

                                    final String url = "https://api.github.com/users/"+username;

                                    StringRequest getRequest = new StringRequest(Request.Method.GET, url,
                                            new Response.Listener<String>()
                                            {
                                                @Override
                                                public void onResponse(String response) {
                                                    // display response
                                                    Log.d("Response", response);

                                                    Gson gson = new Gson();

                                                    users user = gson.fromJson(
                                                            response,
                                                            users.class
                                                    );

                                                    for (int i=0;i<rep.size(); i++){
                                                        if ( rep.get(i) != null ) {
                                                            if (rep.get(i).get(Constantes.USERNAME).equals(user.login)) {
                                                                rep.get(i).put(Constantes.FULLNAME, user.name);
                                                            }
                                                        }
                                                    }

                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            javaPopAdapterRV.refreshAdp();
                                                        }
                                                    });

                                                }
                                            },
                                            new Response.ErrorListener()
                                            {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    Log.d("Error.Response", error.toString() );
                                                }
                                            }
                                    );

// add it to the RequestQueue
                                    queue.add(getRequest);


                                }

                                currentReadName = rep.size() - 1;
                            }
                        }).start();


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString() );
                    }
                }
        );

// add it to the RequestQueue
        queue.add(getRequest);

    }

    private void initActions() {

        ItemClickSupport.addTo(rv_listjavapop).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Intent mIntent = new Intent(context, TelaListaPRs.class);
                        HMAux item = (HMAux) rep.get(position);
                        String pr_url = item.get(Constantes.PR_URL); // pull request URL
                        mIntent.putExtra(Constantes.PR_URL, pr_url);
                        startActivity(mIntent);
                    }
                }
        );

        rv_listjavapop.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == rep.size() - 1) {
                        //bottom of list!
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore() {
        rep.add(null);
        javaPopAdapterRV.notifyItemInserted(rep.size() - 1);
        gerarReps();

    }

    private void cleanOnProgress() {
        if (rep.size() != 0) {
            if (rep.get(rep.size() - 1) == null) {
                rep.remove(rep.size() - 1);
                int scrollPosition = rep.size();
                javaPopAdapterRV.notifyItemRemoved(scrollPosition);
            }
        }
    }
}
