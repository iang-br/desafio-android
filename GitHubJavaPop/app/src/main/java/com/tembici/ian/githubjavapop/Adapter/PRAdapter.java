package com.tembici.ian.githubjavapop.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tembici.ian.githubjavapop.R;
import com.tembici.ian.githubjavapop.Util.Auxiliar;
import com.tembici.ian.githubjavapop.Util.Constantes;
import com.tembici.ian.githubjavapop.Util.HMAux;

import java.util.List;

public class PRAdapter extends BaseAdapter {

    private Context context;

    private int resource; // Layout célula
    private List<HMAux> dados; //coleção

    private LayoutInflater mInflater; // Le XML transforma em um Binario findViewByID

    public void refreshAdp() {
        notifyDataSetChanged();
    }

    public PRAdapter(Context context, int resource, List<HMAux> dados) {
        this.context = context;
        this.resource = resource;
        this.dados = dados;
        //
        this.mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return dados.size();
    }

    @Override
    public Object getItem(int position) {
        return dados.get(position);
    }

    @Override
    public long getItemId(int position) {
        HMAux aux = dados.get(position);
        //
        return Long.parseLong(aux.get(Constantes.ID));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(resource, parent, false);
        }

        // Puxar os Dados de uma posicao especifica
        HMAux item = dados.get(position);

        // Acesso todos os controles da celula
        ImageView iv_photo = convertView.findViewById(R.id.celpr_iv_photo);
        TextView tv_body = convertView.findViewById(R.id.celpr_tv_body);
        TextView tv_fullname = convertView.findViewById(R.id.celpr_tv_fullname);
        TextView tv_title = convertView.findViewById(R.id.celpr_tv_title);
        TextView tv_username = convertView.findViewById(R.id.celpr_tv_username);
        TextView tv_date = convertView.findViewById(R.id.celpr_tv_date);

        String username = item.get(Constantes.USERNAME);
        iv_photo.setImageBitmap(Auxiliar.LoadImage(context,username));
        tv_title.setText(item.get(Constantes.TITLE));
        tv_username.setText(username);
        tv_fullname.setText(item.get(Constantes.FULLNAME));
        tv_body.setText(item.get(Constantes.BODY));
        tv_date.setText(item.get(Constantes.DATE));


        return convertView;
    }
}
