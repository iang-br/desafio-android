package com.tembici.ian.githubjavapop.Json.api_pulls;

import com.tembici.ian.githubjavapop.Util.Auxiliar;
import com.tembici.ian.githubjavapop.Util.Constantes;
import com.tembici.ian.githubjavapop.Util.HMAux;

/**
 * Created by nalmir on 25/07/2018.
 */

public class pulls {

    private String id;
    private String title;
    private user user;
    private String body;
    private String state;
    private String html_url;
    private String created_at;

    public HMAux getPRHashmap(){
        HMAux item = new HMAux();
        item.put(Constantes.ID,String.valueOf(id));
        item.put(Constantes.TITLE,title);
        item.put(Constantes.BODY,body);
        item.put(Constantes.USERNAME,user.getLogin());
        item.put(Constantes.PHOTO_URL,user.getAvatar_url());
        item.put(Constantes.HTML_URL,html_url);
        item.put(Constantes.DATE, Auxiliar.formatDate(created_at));

        return item;
    };



}
