package com.tembici.ian.githubjavapop.View;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tembici.ian.githubjavapop.Util.Auxiliar;
import com.tembici.ian.githubjavapop.Util.Constantes;
import com.tembici.ian.githubjavapop.Util.HMAux;
import com.tembici.ian.githubjavapop.Json.api_pulls.pulls;
import com.tembici.ian.githubjavapop.Json.api_users.users;
import com.tembici.ian.githubjavapop.Adapter.PRAdapter;
import com.tembici.ian.githubjavapop.R;

import java.util.ArrayList;

public class TelaListaPRs extends AppCompatActivity {

    private Context context;

    private ListView lv_listprs;
    private com.tembici.ian.githubjavapop.Adapter.PRAdapter PRAdapter;
    private ArrayList<HMAux> prs;

    private RequestQueue queue;

    private String pr_url;
    private int iterate_fullnames;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.telalistaprs);

        initVars();
        initActions();
    }

    private void initVars() {
        queue = Volley.newRequestQueue(this);

        pr_url = getIntent().getStringExtra(Constantes.PR_URL);


        context = getBaseContext();

        lv_listprs = findViewById(R.id.telalistaprs_lv_listprs);

        gerarReps(100);

        PRAdapter = new PRAdapter(
                context,
                R.layout.celpr,
                prs
        );

        lv_listprs.setAdapter(PRAdapter);

    }

    private void gerarReps(int quantidade) {
        prs = new ArrayList<>(); // colecao vazia

        // prepare the Request
        StringRequest getRequest = new StringRequest(Request.Method.GET, pr_url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // display response
                        Log.d("Response", response);

                        ArrayList<pulls> pulls = (new Gson()).fromJson(
                                response,
                                new TypeToken<ArrayList<pulls>>() {}.getType()
                        );

                        for (int i=0; i< pulls.size(); i++) {
                            HMAux Item = pulls.get(i).getPRHashmap();
                            prs.add(Item);
                        }

                        PRAdapter.refreshAdp();

                        // Thread com objetivo de car
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                for (int i=0; i< prs.size(); i++) {
                                    String photo_url = prs.get(i).get(Constantes.PHOTO_URL);
                                    String username = prs.get(i).get(Constantes.USERNAME);
                                    Auxiliar.DownloadAndSaveImageFromWeb(context, photo_url, username);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            PRAdapter.refreshAdp();
                                        }
                                    });
                                }
                            }
                        }).start();

                        // Thread com objetivo de buscar os fullnames dos clientes
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                for (iterate_fullnames=0; iterate_fullnames< prs.size(); iterate_fullnames++) {

                                    String username = prs.get(iterate_fullnames).get(Constantes.USERNAME);

                                    final String url = "https://api.github.com/users/"+username;

                                    StringRequest getRequest = new StringRequest(Request.Method.GET, url,
                                            new Response.Listener<String>()
                                            {
                                                @Override
                                                public void onResponse(String response) {
                                                    // display response
                                                    Log.d("Response", response);

                                                    Gson gson = new Gson();

                                                    users user = gson.fromJson(
                                                            response,
                                                            users.class
                                                    );

                                                    for (int i=0;i<prs.size(); i++){
                                                        if ( prs.get(i).get(Constantes.USERNAME).equals(user.login) ) {
                                                            prs.get(i).put(Constantes.FULLNAME, user.name);
                                                        }
                                                    }

                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            PRAdapter.refreshAdp();
                                                        }
                                                    });

                                                }
                                            },
                                            new Response.ErrorListener()
                                            {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    Log.d("Error.Response", error.toString() );
                                                }
                                            }
                                    );

// add it to the RequestQueue
                                    queue.add(getRequest);


                                }
                            }
                        }).start();



                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                }
        );

        // add it to the RequestQueue
        queue.add(getRequest);
    }

    private void initActions() {
        lv_listprs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Uri uri = Uri.parse(prs.get(i).get(Constantes.HTML_URL));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }
}
